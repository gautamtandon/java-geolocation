# Geolocation service #

Geolocation is used to add *location awareness* into an application. This is typically achieved in web browsers using the `window.navigator` object. W3C provides Geolocation API Specification Recommendation and all (or almost all) browsers follow this recommendation. However many times similar *location awareness* is needed in applications other than web browsers (such as server side or stand alone application). While there are a number of geolocation services available for this, none of them follow the API Specifications recommended by W3C. The aim of this geolocation service is to provide a sample Java implementation of geolocation service based on the Geolocation API Specifications recommended by W3C, which can be used in Java applications other than web browsers.

Here I have tried to demonstrate how a pure Java "Non-Browser" implementation of Geolocation Service can be written that follows W3C Specifications.

Geolocation API Specification Recommendation proposed by W3C on May 10th, 2012 is available here: [http://www.w3.org/TR/2012/PR-geolocation-API-20120510/](http://www.w3.org/TR/2012/PR-geolocation-API-20120510/)

## Usage ##
		// Create instance of a "Navigator" object that uses IP Address based 
		// geolocation awareness.
		Navigator nav = new Navigator(new IPAddressBasedGeoAcquirer());
		
		// Get an instance of NavigatorGeolocation from it.
		NavigatorGeolocation navGeo = (NavigatorGeolocation)nav;
		
		// Get the geolocation by passing your Position call back
		// Optionally you may set the PositionOptions to null
		navGeo.getGeolocation().getCurrentPosition(new PositionCallback() {
			
			public void onSuccess(Position position) {
				System.out.println(position.getCoords().getLatitude() + " : " + position.getCoords().getLongitude());
			}
			
			public void onFailure(PositionError positionError) {
				System.out.println(positionError.getCode() + " :: " + positionError.getMessage());
			}
		}, null);

## Adding your own Geolocation Awareness Impmentation ##
Currently I am using `http://ifconfig.me/ip` to get application's IP Address. Then I use `http://freegeoip.net` to get geolocation information based on that IP Address. The relevant code is in `gt.geolocation.IPAddressBasedGeoAcquirer` class. You can use your own logic by writing your own class that extends `gt.geolocation.GeolocationAcquirer`. 

## Security and privacy considerations ##

In order to conform to the W3C Specification, the calling application of this service must provide a mechanism that protects the user's privacy and this mechanism should ensure that no location information is made available without the user's express permission.