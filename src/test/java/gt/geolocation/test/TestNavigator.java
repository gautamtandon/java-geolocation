/**
 * The MIT License (MIT)
 * 
 * Copyright (c) 2013 Gautam Tandon (mail@gautamtandon.me)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy 
 * of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights 
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
 * copies of the Software, and to permit persons to whom the Software is 
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in 
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN 
 * THE SOFTWARE.
 */

package gt.geolocation.test;

import gt.geolocation.IPAddressBasedGeoAcquirer;
import gt.geolocation.w3c.Navigator;
import gt.geolocation.w3c.NavigatorGeolocation;
import gt.geolocation.w3c.Position;
import gt.geolocation.w3c.PositionCallback;
import gt.geolocation.w3c.PositionError;
import gt.geolocation.w3c.PositionOptions;

public class TestNavigator {

	public static void main(String[] args) {
		PositionOptions posO = new PositionOptions();
		posO.setTimeout(1000);

		Navigator nav = new Navigator(new IPAddressBasedGeoAcquirer());
		
		NavigatorGeolocation navGeo = (NavigatorGeolocation)nav;
		
		navGeo.getGeolocation().getCurrentPosition(new PositionCallback() {
			
			public void onSuccess(Position position) {
				System.out.println(position.getCoords().getLatitude() + " : " + position.getCoords().getLongitude());
			}
			
			public void onFailure(PositionError positionError) {
				System.out.println(positionError.getCode() + " :: " + positionError.getMessage());
			}
		}, null);
	}
}
